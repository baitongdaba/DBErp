# laminas-hydrator

[![Build Status](https://github.com/laminas/laminas-hydrator/actions/workflows/continuous-integration.yml/badge.svg)](https://github.com/laminas/laminas-hydrator/actions/workflows/continuous-integration.yml)
[![Psalm coverage](https://shepherd.dev/github/laminas/laminas-hydrator/coverage.svg?)](https://shepherd.dev/github/laminas/laminas-hydrator)


`Laminas\Hydrator` provides utilities for mapping arrays to objects, and vice
versa, including facilities for filtering which data is mapped as well as
providing mechanisms for mapping nested structures.

- File issues at https://github.com/laminas/laminas-hydrator/issues
- Documentation is at https://docs.laminas.dev/laminas-hydrator/

<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

$baseFramework = [
    'Laminas\Serializer',
    'Laminas\InputFilter',
    'Laminas\Filter',
    'Laminas\Hydrator',
    'Laminas\Paginator',
    'Laminas\ServiceManager\Di',
    'Laminas\Session',
    'Laminas\Mvc\Plugin\Prg',
    'Laminas\Mvc\Plugin\Identity',
    'Laminas\Mvc\Plugin\FlashMessenger',
    'Laminas\Mvc\Plugin\FilePrg',
    'Laminas\Mvc\I18n',
    'Laminas\I18n',
    'Laminas\Mvc\Console',
    'Laminas\Log',
    'Laminas\Form',
    'Laminas\Db',
    'Laminas\Cache',
    'Laminas\Router',
    'Laminas\Validator',

    'DoctrineModule',
    'DoctrineORMModule'
];

$erpModule = ['Install'];
if (file_exists('data/install.lock')) {
    $erpModule = [
        'Admin',    //管理
        'Store',    //基础信息
        'Customer', //客户
        'Purchase', //采购
        'Finance',  //资金
        'Sales',    //销售
        'Api',      //api
        'Shop',     //商城
        'Report',   //报告
        'Stock',    //库存
        'Extend',   //扩展
        'Install'   //安装
    ];

    //插件
    if (file_exists('data/moduleData/Plugin/plugin.module.php')) {
        $pluginModule = require 'data/moduleData/Plugin/plugin.module.php';
        if (!empty($pluginModule)) $erpModule = array_merge($erpModule, $pluginModule);
    }
}

return array_merge($baseFramework, $erpModule);

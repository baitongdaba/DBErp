<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Store\Service;

use Doctrine\ORM\EntityManager;
use Sales\Entity\SalesOrderGoodsReturn;
use Store\Entity\GoodsSerialNumber;
use function Doctrine\ORM\QueryBuilder;

class GoodsSerialNumberManager
{
    private $entityManager;

    public function __construct(
        EntityManager $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    /**
     * 添加序列号
     * @param array $data
     * @return GoodsSerialNumber
     * @throws \Doctrine\ORM\Exception\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addSerialNumber(array $data): GoodsSerialNumber
    {
        $serialNumber = new GoodsSerialNumber();
        $serialNumber->valuesSet($data);

        $this->entityManager->persist($serialNumber);
        $this->entityManager->flush();

        return $serialNumber;
    }

    /**
     * 编辑序列号
     * @param array $data
     * @param GoodsSerialNumber $goodsSerialNumber
     * @return GoodsSerialNumber
     * @throws \Doctrine\ORM\Exception\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function editSerialNumber(array $data, GoodsSerialNumber $goodsSerialNumber): GoodsSerialNumber
    {
        $goodsSerialNumber->valuesSet($data);
        $this->entityManager->flush();

        return $goodsSerialNumber;
    }

    /**
     * 商品销售序列号退货
     * @param $salesOrderReturnId
     * @param $salesOrderId
     * @return void
     */
    public function salesOrderGoodsReturn($salesOrderReturnId, $salesOrderId)
    {
        $nowTime = time();

        $returnSalesGoods = $this->entityManager->getRepository(SalesOrderGoodsReturn::class)->findBy(['salesOrderReturnId' => $salesOrderReturnId]);
        if ($returnSalesGoods) foreach ($returnSalesGoods as $returnGoodsValue) {
            if (empty($returnGoodsValue->getGoodsSerialNumberStr())) continue;
            $qb = $this->entityManager->createQueryBuilder();
            $qb->update(GoodsSerialNumber::class, 'n')
                ->set('n.serialNumberState', ':serialNumberState')->setParameter('serialNumberState', 3)
                ->set('n.returnType', ':returnType')->setParameter('returnType', 2)
                ->set('n.returnTime', ':returnTime')->setParameter('returnTime', $nowTime)
                ->where($qb->expr()->in('n.serialNumber', explode(',', $returnGoodsValue->getGoodsSerialNumberStr())))
                ->andWhere('n.serialNumberType = :serialNumberType')->setParameter('serialNumberType', 4)
                ->andWhere('n.outboundInId = :outboundInId')->setParameter('outboundInId', $salesOrderId);
            $qb->getQuery()->execute();
        }
    }
}
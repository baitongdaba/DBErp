<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Store\View;

use Admin\Data\Common;
use Laminas\Mvc\I18n\Translator;
use Laminas\View\Helper\AbstractHelper;

class StoreHelper extends AbstractHelper
{
    private $translator;

    public function __construct(
        Translator  $translator
    )
    {
        $this->translator   = $translator;
    }

    /**
     * 商品序列号状态
     * @param $state
     * @return mixed
     */
    public function serialNumberState($state)
    {
        return Common::serialNumberState($this->translator)[$state];
    }

    /**
     * 商品图片
     * @param $image
     * @return string
     */
    public function goodsImage($image): string
    {
        if (empty($image)) return '';

        return $this->getView()->basePath($image);
    }
}
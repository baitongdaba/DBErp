<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Store\Controller;

use Admin\Data\Common;
use Doctrine\ORM\EntityManager;
use Laminas\Filter\StripTags;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\I18n\Translator;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;
use Store\Entity\GoodsSerialNumber;
use Store\Form\SearchGoodsSerialNumberForm;

class GoodsSerialNumberController extends AbstractActionController
{
    private $translator;
    private $entityManager;

    public function __construct(
        Translator $translator,
        EntityManager $entityManager
    )
    {
        $this->translator       = $translator;
        $this->entityManager    = $entityManager;
    }

    public function indexAction()
    {
        $page = (int) $this->params()->fromQuery('page', 1);

        $search = [];
        $searchForm = new SearchGoodsSerialNumberForm();
        $searchForm->get('serial_number_state')->setValueOptions(array_merge([0 => $this->translator->translate('选择状态')], Common::serialNumberState($this->translator)));
        if($this->getRequest()->isGet()) {
            $data = $this->params()->fromQuery();
            $searchForm->setData($data);
            if($searchForm->isValid()) $search = $searchForm->getData();
        }

        $query = $this->entityManager->getRepository(GoodsSerialNumber::class)->goodsSerialNumberList($search);
        $goodsSerialList = $this->adminCommon()->erpPaginator($query, $page);

        return ['goodsSerialList' => $goodsSerialList, 'searchForm' => $searchForm];
    }

    /**
     * ajax获取商品序列号列表
     * @return ViewModel
     */
    public function ajaxGoodsSerialNumberSearchAction(): ViewModel
    {
        $view = new ViewModel();
        $view->setTerminal(true);

        $page = (int) $this->params()->fromQuery('page', 1);

        $searchGoodsSerialNumber = trim($this->params()->fromQuery('searchGoodsSerialNumber'));
        if(!empty($searchGoodsSerialNumber)) {
            $filter = new StripTags();
            $searchGoodsSerialNumber = $filter->filter($searchGoodsSerialNumber);
        }
        $goodsId = intval($this->params()->fromQuery('goodsId'));
        $searchWarehouseId = intval($this->params()->fromQuery('searchWarehouseId'));

        $query = $this->entityManager->getRepository(GoodsSerialNumber::class)->ajaxFindAllGoodsSerialNumber($goodsId, $searchGoodsSerialNumber, $searchWarehouseId);
        $goodsSerialNumberList = $this->adminCommon()->erpPaginator($query, $page);

        return $view->setVariables([
            'goodsSerialNumberList'     => $goodsSerialNumberList,
            'searchGoodsSerialNumber'   => $searchGoodsSerialNumber,
            'goodsId'                   => $goodsId,
            'warehouseId'               => $searchWarehouseId
        ]);
    }

    /**
     * 计算序列号数量
     * @return JsonModel
     */
    public function ajaxGoodsSerialNumberAction(): JsonModel
    {
        $serialNumberStr = $this->params()->fromPost('serialGoodsNumberStr','');
        if (empty($serialNumberStr)) $serialNum = 0;
        else $serialNum = count(array_filter(explode("\n", $serialNumberStr)));

        return new JsonModel(['state' => 'ok', 'serialNumberNum' => $serialNum]);
    }
}
<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Stock\Controller;

use Admin\Data\Common;
use Doctrine\ORM\EntityManager;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\I18n\Translator;
use Store\Entity\Goods;
use Store\Form\SearchGoodsForm;

class StockWarningController extends AbstractActionController
{
    private $translator;
    private $entityManager;

    public function __construct(
        Translator $translator,
        EntityManager $entityManager
    )
    {
        $this->translator   = $translator;
        $this->entityManager= $entityManager;
    }

    public function indexAction()
    {
        $page = (int) $this->params()->fromQuery('page', 1);

        $search = [];
        $searchForm = new SearchGoodsForm();
        $searchForm->get('goods_category_id')->setValueOptions($this->storeCommon()->categoryListOptions($this->translator->translate('商品分类')));
        $searchForm->get('brand_id')->setValueOptions($this->storeCommon()->brandListOptions($this->translator->translate('商品品牌')));
        if($this->getRequest()->isGet()) {
            $data = $this->params()->fromQuery();
            $searchForm->setData($data);
            if($searchForm->isValid()) $search = $searchForm->getData();
        }

        $warningMinNum = Common::configValue('other', 'config')['stock_min_warning'] ?? 0;
        $warningMaxNum = Common::configValue('other', 'config')['stock_max_warning'] ?? 0;

        $query      = $this->entityManager->getRepository(Goods::class)->findStockWarningGoodsList($warningMinNum, $warningMaxNum, $search);
        $goodsList  = $this->adminCommon()->erpPaginator($query, $page);

        return [
            'goodsList' => $goodsList,
            'searchForm' => $searchForm,
            'warningMinNum' => $warningMinNum > 0 ? $warningMinNum : $this->translator->translate('无限制'),
            'warningMaxNum' => $warningMaxNum > 0 ? $warningMaxNum : $this->translator->translate('无限制')
        ];
    }
}
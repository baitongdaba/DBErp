<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Admin\Form;

use Admin\Data\Config;
use Admin\Filter\ImageRegenerate;
use Admin\Validator\ImageSuffixValidator;
use Laminas\Form\Form;

class SystemForm extends Form
{
    public function __construct($name = 'system-form', array $options = [])
    {
        parent::__construct($name, $options);

        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('class', 'form-horizontal');

        $this->addElements();
        $this->addInputFilter();
    }

    public function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name'  => 'company_name|base',
            'attributes'    => [
                'id'            => 'company_name|base',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'file',
            'name'  => 'company_logo|base',
            'attributes'    => [
                'id'            => 'company_logo|base',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'website_icp|base',
            'attributes'    => [
                'id'            => 'website_icp|base',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'checkbox',
            'name'  => 'shop_service|base',
            'attributes' => [
                'value' => 1
            ]
        ]);

        $this->add([
            'type'  => 'select',
            'name'  => 'currency_code|base',
            'attributes'    => [
                'id'        => 'currency_code|base',
                'class'     => 'form-control select2'
            ]
        ]);

        $this->add([
            'type'  => 'select',
            'name'  => 'website_timezone|base',
            'attributes'    => [
                'id'            => 'website_timezone|base',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'company_tax_number|base',
            'attributes'    => [
                'id'            => 'company_tax_number|base',
                'class'         => 'form-control'
            ]
        ]);
        $this->add([
            'type'  => 'text',
            'name'  => 'company_address|base',
            'attributes'    => [
                'id'            => 'company_address|base',
                'class'         => 'form-control'
            ]
        ]);
        $this->add([
            'type'  => 'text',
            'name'  => 'company_phone|base',
            'attributes'    => [
                'id'            => 'company_phone|base',
                'class'         => 'form-control'
            ]
        ]);
        $this->add([
            'type'  => 'text',
            'name'  => 'company_bank|base',
            'attributes'    => [
                'id'            => 'company_bank|base',
                'class'         => 'form-control'
            ]
        ]);
        $this->add([
            'type'  => 'text',
            'name'  => 'company_bank_account|base',
            'attributes'    => [
                'id'            => 'company_bank_account|base',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'goods|code',
            'attributes'    => [
                'id'            => 'goods|code',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'category|code',
            'attributes'    => [
                'id'            => 'category|code',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'brand|code',
            'attributes'    => [
                'id'            => 'brand|code',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'warehouse|code',
            'attributes'    => [
                'id'            => 'warehouse|code',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'stock_in|code',
            'attributes'    => [
                'id'            => 'stock_in|code',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'stock_ex|code',
            'attributes'    => [
                'id'            => 'stock_ex|code',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'stock_check|code',
            'attributes'    => [
                'id'            => 'stock_check|code',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'stock_transfer|code',
            'attributes'    => [
                'id'            => 'stock_transfer|code',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'purchase_order|code',
            'attributes'    => [
                'id'            => 'purchase_order|code',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'warehouse_order|code',
            'attributes'    => [
                'id'            => 'warehouse_order|code',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'sales_order|code',
            'attributes'    => [
                'id'            => 'sales_order|code',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'sales_send_order|code',
            'attributes'    => [
                'id'            => 'sales_send_order|code',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'customer|code',
            'attributes'    => [
                'id'            => 'customer|code',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'customer_category|code',
            'attributes'    => [
                'id'            => 'customer_category|code',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'supplier|code',
            'attributes'    => [
                'id'            => 'supplier|code',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'supplier_category|code',
            'attributes'    => [
                'id'            => 'supplier_category|code',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'checkbox',
            'name'  => 'login_captcha|other',
            'attributes' => [
                'value' => 0
            ]
        ]);

        $this->add([
            'type'  => 'number',
            'name'  => 'stock_min_warning|other',
            'attributes'    => [
                'id'    => 'stock_min_warning|other',
                'class' => 'form-control',
                'value' => 0
            ]
        ]);

        $this->add([
            'type'  => 'number',
            'name'  => 'stock_max_warning|other',
            'attributes'    => [
                'id'    => 'stock_max_warning|other',
                'class' => 'form-control',
                'value' => 0
            ]
        ]);

        $this->add([
            'type'  => 'number',
            'name'  => 'image_width|other',
            'attributes'    => [
                'id'    => 'image_width|other',
                'class' => 'form-control',
                'value' => 50
            ]
        ]);
    }

    public function addInputFilter()
    {
        $inputFilter = $this->getInputFilter();

        $inputFilter->add([
            'name'      => 'company_name|base',
            'required'  => true,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'type'      => 'Laminas\InputFilter\FileInput',
            'name'      => 'company_logo|base',
            'required'  => false,
            'validators'=> [
                ['name' => 'FileUploadFile'],
                ['name' => ImageSuffixValidator::class],
                [
                    'name' => 'FileMimeType',
                    'options' => [
                        'mimeType' => ['image/jpeg', 'image/png']
                    ]
                ],
                ['name' => 'FileIsImage'],
                [
                    'name' => 'FileSize',
                    'options' => [
                        'min' => '1kB',
                        'max' => '8MB'
                    ]
                ]
            ],
            'filters' => [
                [
                    'name' => 'FileRenameUpload',
                    'options' => [
                        'target' => 'public/upload/common/logo.png',
                        'overwrite'=>true
                    ]
                ],
                ['name' => ImageRegenerate::class]
            ]
        ]);

        $inputFilter->add([
            'name'      => 'website_icp|base',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'shop_service|base',
            'required'  => false,
            'validators'=> [
                [
                    'name'      => 'InArray',
                    'options'   => [
                        'haystack'  => [0, 1]
                    ]
                ]
            ]
        ]);

        $inputFilter->add([
            'name'      => 'currency_code|base',
            'required'  => true,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'website_timezone|base',
            'required'  => false
        ]);

        $inputFilter->add([
            'name'      => 'company_tax_number|base',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);
        $inputFilter->add([
            'name'      => 'company_address|base',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);
        $inputFilter->add([
            'name'      => 'company_phone|base',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);
        $inputFilter->add([
            'name'      => 'company_bank|base',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);
        $inputFilter->add([
            'name'      => 'company_bank_account|base',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'goods|code',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'category|code',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'brand|code',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'warehouse|code',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'stock_in|code',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'stock_ex|code',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ]
        ]);

        $inputFilter->add([
            'name'      => 'stock_check|code',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ]
        ]);

        $inputFilter->add([
            'name'      => 'stock_transfer|code',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ]
        ]);

        $inputFilter->add([
            'name'      => 'purchase_order|code',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ]
        ]);

        $inputFilter->add([
            'name'      => 'warehouse_order|code',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ]
        ]);

        $inputFilter->add([
            'name'      => 'sales_order|code',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ]
        ]);

        $inputFilter->add([
            'name'      => 'sales_send_order|code',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ]
        ]);

        $inputFilter->add([
            'name'      => 'customer|code',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ]
        ]);

        $inputFilter->add([
            'name'      => 'customer_category|code',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ]
        ]);

        $inputFilter->add([
            'name'      => 'supplier|code',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ],
        ]);

        $inputFilter->add([
            'name'      => 'supplier_category|code',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ],
        ]);

        $inputFilter->add([
            'name'      => 'login_captcha|other',
            'required'  => false,
            'validators'=> [
                [
                    'name'      => 'InArray',
                    'options'   => [
                        'haystack'  => [0, 1]
                    ]
                ]
            ]
        ]);

        $inputFilter->add([
            'name'      => 'image_width|other',
            'required'  => false,
            'filters'   => [
                ['name' => 'ToInt']
            ],
            'validators'=> [
                [
                    'name'      => 'GreaterThan',
                    'options'   => [
                        'min'   => 0
                    ]
                ]
            ]
        ]);
    }
}